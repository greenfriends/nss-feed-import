<?php
require ('header.php');
?>


<?php foreach (\SUPPLIERS as $supplier):
if (in_array($supplier['supplierId'], $ignored)) { continue; } ?>
<?php
    $errors = unserialize($redis->get(sprintf('importFeed:%s:#updateErrors', \SUPPLIERS[$supplier['supplierId']]['name'])));
    $drafted = $redis->get(sprintf('importFeed:%s:#updateDrafted', \SUPPLIERS[$supplier['supplierId']]['name']));
    $skipped = $redis->get(sprintf('importFeed:%s:#updateErrorsCount', \SUPPLIERS[$supplier['supplierId']]['name']));
    $newItems = $redis->get(sprintf('importFeed:%s:#updateNewItems', \SUPPLIERS[$supplier['supplierId']]['name']));
    $matchedItems = $redis->get(sprintf('importFeed:%s:#updateExistingItems', \SUPPLIERS[$supplier['supplierId']]['name']));
    $totalSourceItems = $redis->get(sprintf('importFeed:%s:#updateTotalSourceItems', \SUPPLIERS[$supplier['supplierId']]['name']));
    $updateStat = $redis->get(sprintf('importFeed:%s:#updateStat', \SUPPLIERS[$supplier['supplierId']]['name']));
    $createStat = $redis->get(sprintf('importFeed:%s:#createStat', \SUPPLIERS[$supplier['supplierId']]['name']));
    $time = $redis->get(sprintf('importFeed:%s:#updateTime', \SUPPLIERS[$supplier['supplierId']]['name']));

    if (!$totalSourceItems) { continue; }
    //@TODO created items are not reset
?>
<div>
    <h2><u><?= $supplier['name']; ?></u></h2>

    <p>Last update was @ <?=$time?>, with <?=$updateStat?> existing items.</p>
    <?php if ((int) $drafted > 0): ?>
    <p><?=(int) $drafted?> items was drafted (not in feed anymore).</p>
    <?php endif;?>
    <p><?=$newItems?> new items in feed.</p>
    <p><?=$matchedItems?> existing items in feed.</p>
    <p><?=$totalSourceItems?> total items in feed.</p>

    <p>Errors</p>
    <p>There was a total of <?=$skipped?> skipped items during parsing</p>
    <?php foreach ($errors as $error): ?>
    <p><?=$error?></p>
    <?php endforeach; ?>

</div>
<?php endforeach;?>