<?php require ('header.php'); ?>
<div class="filterWrapper">
    <label>
        Filtriraj po dobavljaču
        <select id="supplierFilter" name="supplierId">
            <option value="-1">Izaberite dobavljača</option>
        <?php foreach (SUPPLIERS as $supplier):?>
        <option value="<?=$supplier['supplierId']?>"><?=$supplier['name']?></option>
        <?php endforeach;?>
        </select>
    </label>
</div>
<table id="logtable" class="stripe cell-border">
    <thead>
    <tr>
        <th class="no-sort">Id loga</th>
        <th class="no-sort">Id proizvoda</th>
        <th class="no-sort">Dobavljac</th>
        <th class="no-sort">Tip proizvoda</th>
        <th class="no-sort">Vrsta promene</th>
        <th class="no-sort">Poruka</th>
        <th class="no-sort">Stare vrednosti</th>
        <th class="no-sort">Nove vrednosti</th>
        <th class="no-sort">Datum promene</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script>
    jQuery(document).ready(function () {
        let ajaxUrl = '<?=admin_url( 'admin-ajax.php').'?action=getLogData'?>';
        let ajaxUrlWithFilters;
        let supplierFilter = jQuery('#supplierFilter');
        supplierFilter.on('change', function (e) {
            ajaxUrlWithFilters = ajaxUrl;
            ajaxUrlWithFilters += '&supplierId=' + e.target.value;
            table.ajax.url(ajaxUrlWithFilters);
            table.draw()
        })
        let table = jQuery('#logtable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": ajaxUrl,
            "pageLength": 50,
            "searchable": true,
            'columns': [
                {data: 'logId'},
                {data: 'productId'},
                {data: 'supplierName', className:'supplierInfo'},
                {data: 'type'},
                {data: 'attribute'},
                {data: 'message'},
                {data: 'oldValue'},
                {data: 'newValue'},
                {data: 'timestamp'}
            ],
            drawCallback: function () {
                var api = this.api();
                var drawData = api.ajax.json();
                for (const index in drawData.data){
                    if (index === '0' ) continue;
                    const supplierName = drawData.data[index].supplierName;
                    let column = jQuery('.supplierInfo')[index];
                    column.setAttribute('title', supplierName);
                }
            },
            columnDefs: [{
                orderable: false,
                targets: "no-sort"

            }]
        });
    })
</script>