<?php require ('header.php'); ?>
<div class="filterWrapper">
    <label>
        Mapiranje kategorija za feed
        <select id="supplierFilter" name="supplierId">
            <option value="-1">Izaberite dobavljača</option>
        <?php foreach (\SUPPLIERS as $supplier):?>
        <option value="<?=$supplier['supplierId']?>" <?=($supplier['supplierId'] == @$_GET['supplierId']) ? 'selected':''?>><?=$supplier['name']?></option>
        <?php endforeach;?>
        </select>
    </label>
</div>

<form method="post">
<?php if ($msg): ?>
    <p><strong><?=$msg?></strong></p>
<?php endif; ?>
<table id="logtable" class="stripe cell-border">
    <thead>
    <tr>
        <th colspan="2" class="no-sort">Izvorna kategorija</th>
<!--        <th class="no-sort">Izvorna kategorija 2</th>-->
        <th class="no-sort">Lokalna kategorija</th>
        <th class="no-sort">Lokalna kategorija</th>
    </tr>
    </thead>
    <tbody>
        <?php
        /* @var League\Csv\Reader $catList */
        if ($catList):
        foreach ($catList->getIterator() as $record):  ?>
        <tr>
            <td colspan="2">
                <input readonly name="source1[]" size="60" type="text" value="<?=$record['source1']?>" /> <br />
                <input readonly name="source2[]" size="60" type="text" value="<?=$record['source2']?>" /> <br />&nbsp;
            </td>
            <td><select name="localId1[]" style="width: 350px"><option value="0">-- Izaberi kategoriju --</option>
                <option value="false" <?=('false' == $record['localId1']) ? 'selected':''?>>-- Ignoriši --</option>
                <?php foreach ($categories as $category):
                    $catName = $category->name;
                    if ($category->parent) {
                        $parent = get_term_by('id', $category->parent, 'product_cat');
                        $catName .= ' / ' . $parent->name;
                    }
                    ?>
                <option value="<?=$category->term_id?>" <?=($category->term_id == $record['localId1']) ? 'selected':''?>><?=$catName?></option>
                <?php endforeach; ?></select></td>
            <td><select name="localId2[]" style="width: 350px"><option value="0">-- Izaberi kategoriju --</option>
                <option value="false" <?=('false' == $record['localId2']) ? 'selected':''?>>-- Ignoriši --</option>
                <?php foreach ($categories as $category):
                    $catName = $category->name;
                    if ($category->parent) {
                        $parent = get_term_by('id', $category->parent, 'product_cat');
                        $catName .= ' / ' . $parent->name;
                    }
                    ?>
                    <option value="<?=$category->term_id?>" <?=($category->term_id == $record['localId2']) ? 'selected':''?>><?=$catName?></option>
                <?php endforeach; ?></select></td>
        </tr>
        <?php endforeach; endif;  ?>
    </tbody>
</table>
    <input type="submit" value="Snimi" name="saveMapping" />
</form>

<script>
    jQuery(document).ready(function () {
        let supplierFilter = jQuery('#supplierFilter');
        var urlParams = new URLSearchParams(window.location.search);
        supplierFilter.on('change', function (e) {
            urlParams.set('supplierId', e.target.value);
            window.location.search = urlParams.toString();
        })
    })
</script>