<?php

namespace Nss\Feed;

use Nss\Feed\Parser\Prometz;

class Updater
{
    private $imageHandler;

    private $delta = [];

    public function __construct()
    {
        $this->imageHandler = new ImageHandler();
    }

    /**
     * Update product data when item is updated.
     *
     * @param \WC_Product $product
     * @param Product $productData
     * @return \WC_Product
     */
    public function updateWcProduct(Product $productData)
    {
        global $wpdb;

        $this->delta = [];

        /* @var \WC_Product $product */
        $product = wc_get_product($productData->getPostId());
        if (!$product) {
            throw new \Exception(sprintf('product with id %s and vendorcode %s not found.', $productData->getPostId(), $productData->getSupplierSku()));
        }

        $this->updateSimplePrices($productData, $product);
        $this->updateStatus($productData, $product);
        $this->updateOtherAttributes($productData, $product);
        // prometz
        if ($productData->getSupplierId() === 230932) {
            $this->updateImage($productData, $product);
        }

        //update category where required
        // @TODO don't remove special categories
        if (in_array($productData->getSupplierId(), [193, 27, 230932, 119])) {
            $categories = explode(',', $productData->getCategoryIds());
            if (count($categories) === 0) {
                throw new \Exception('no categories ' . $productData->getCategoryIds());
            }
            if (count(array_diff($categories, $product->get_category_ids()))) {
                $oldCats = implode(',', $product->get_category_ids());
                $product->set_category_ids($categories);
                $product->save();
                $newCats = implode(',', $product->get_category_ids());
                $this->setDelta($product, $oldCats, $newCats, 'category', 'updating categories');
            }
        }

        if (count($this->delta) > 0) {
            foreach ($this->delta as $postId => $data) {
                $sql = sprintf("INSERT INTO wp_nss_feed_log (productId, supplierId, type, attribute, oldValue, newValue, message, timestamp) VALUES (
        {$postId},'{$data['supplierId']}', '{$data['type']}','{$data['attribute']}', '{$data['old']}', '{$data['new']}', '{$data['message']}', NOW()
)");
                if (!$wpdb->query($sql)) {
                    var_dump($wpdb->last_error);
                }
            }
        }

        return $product;
    }

    private function updateImage(Product $productData, \WC_Product $product)
    {
        $sourceImages = explode(',', $productData->getImages());
        // if main image name is changed, or there are more images for gallery
        if (basename(wp_get_original_image_path($product->get_image_id())) !== basename($sourceImages[0]) ||
            (count($sourceImages)-1) > count($product->get_gallery_image_ids())) {
            $this->imageHandler->handleImage($productData->getImages(), $product->get_id());
            $this->setDelta($product, '!old-value!', 'images updated', 'image', 'updating images');
        }
    }

    private function updateOtherAttributes(Product $productData, \WC_Product $product)
    {
        $updateFor = [Prometz::SUPPLIER_ID];
        if (in_array($productData->getSupplierId(), $updateFor)) {
            $save = false;
            if ($productData->getName() != $product->get_name()) {
                $old = $product->get_name();
                $product->set_name($productData->getName());
                $this->setDelta($product, $old, $productData->getName(), 'name', 'updating name');
                $save = true;
            }
            if ($productData->getDescription() !== '' && $productData->getDescription() != $product->get_description()) {
                $old = $product->get_description();
                $product->set_description($productData->getDescription());
                $this->setDelta($product, $old, $productData->getDescription(), 'description', 'updating description');
                $save = true;
            }
            if ($productData->getWeight() != $product->get_weight()) {
                $old = $product->get_weight();
                $product->set_weight($productData->getWeight());
                $this->setDelta($product, $old, $productData->getWeight(), 'weight', 'updating global weight');
                $save = true;
            }
            if ($save) {
                $product->save();
            }
        }
    }

    private function updateSimplePrices(Product $productData, \WC_Product $product)
    {
        if (get_class($product) == \WC_Product_Variable::class) {
            // seems it cannot be done this way
        } else {
            if ($productData->getRegularPrice() != $product->get_regular_price()
              ||  $productData->getSalePrice() != $product->get_sale_price()) {
                if ($productData->getRegularPrice() == '0' || (int) $productData->getRegularPrice() === 0) {
                    $product->set_status('draft');
                    $product->set_stock_status('outofstock');
//                    var_dump($productData);
//                    throw new \Exception('there was a problem with the price');
                }
                $old = 'price: ' . $product->get_regular_price() .' | sale price: ' . $product->get_sale_price();
                $new = 'price: ' . $productData->getRegularPrice() .' | sale price: ' . $productData->getSalePrice();
                $this->setDelta($product, $old, $new, 'price', 'simple product price');
                $product->set_regular_price($productData->getRegularPrice());
                $product->set_sale_price($productData->getSalePrice());
                $product->save();
            }
//            else {
//                $product->set_sale_price('');
//                $product->save();
//            }
        }
    }

    private function setDelta($product, $old, $new, $attribute, $message = '')
    {
        $type = 'simple';
        if (get_class($product) === \WC_Product_Variable::class) {
            $type = 'variable';
        }
        $this->delta[$product->get_id()]['old'] = $old;
        $this->delta[$product->get_id()]['new'] = $new;
        $this->delta[$product->get_id()]['attribute'] = $attribute;
        $this->delta[$product->get_id()]['type'] = $type;
        $this->delta[$product->get_id()]['message'] = $message;
        $this->delta[$product->get_id()]['supplierId'] = get_post_meta($product->get_id(),'supplier',true);
    }

    /**
     * Will update price as well for variable products
     *
     * @param Product $productData
     * @param \WC_Product $product
     * @return mixed
     * @throws \Exception
     */
    private function updateStatus(Product $productData, \WC_Product $product)
    {
        if (get_class($product) == \WC_Product_Variable::class) {
            if ($product->get_status() === 'draft' && $productData->getStatus() === 'publish') {
                $product->set_status('publish');
                $product->save();
                $this->setDelta($product, 'draft', 'publish', 'status', '!!! published drafted product !!!');
            }

            // @TODO find a way to create new variations here

            foreach ($product->get_available_variations() as $available_variation) {
                $variation = wc_get_product($available_variation['variation_id']);
                if (!is_array($productData->getVelicina())) {
                    var_dump($productData);
                    die('no parsed options found in variable product');
                }

                $boja = null;
                $velicina = null;
                $price = null;
                foreach ($productData->getOptions() as $option) {
                    if (isset($available_variation['attributes']['attribute_pa_boja'])) {
                        if (!isset($option['boja']['value'])) {
                            continue;
                        }
                        $boja = wc_sanitize_taxonomy_name($option['boja']['value']);
                        if ($boja !== $available_variation['attributes']['attribute_pa_boja']) {
                            continue;
                        }
                        $stockStatus = $option['stockStatus'];
                        $price = $option['regularPrice'];
                        $salePrice = $option['salePrice'];
                    }
                    if (isset($available_variation['attributes']['attribute_pa_velicina'])) {
                        if (!isset($option['velicina']['value'])) {
                            continue;
                        }
                        $velicina = wc_sanitize_taxonomy_name($option['velicina']['value']);
                        if ($velicina !== $available_variation['attributes']['attribute_pa_velicina']) {
                            continue;
                        }
                        $stockStatus = $option['stockStatus'];
                        $price = $option['regularPrice'];
                        $salePrice = $option['salePrice'];
                    }
                }

                if (!$velicina && !$boja) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('outofstock');
                    $save = $variation->save();
                    if (is_object($save)) {
                        var_dump('variation stock status to outofstock, item removed from feed');
                        die();
                    }

                    $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' item removed from feed');
//                    $changed[$product->get_id()][$variation->get_id()][] = 'variation stock status to outofstock, item removed from feed';
                    continue;
                }

//                if ($product->get_id() == 473329) {
//                    var_dump($variation->get_stock_status());
//                    var_dump($price);
//                    var_dump($velicina);
//                    var_dump($boja);
//                    die();
//                }

                //variation not found in feed
                if (!isset($price)) {
                    if ($variation->get_stock_status() !== 'outofstock') {
                        $old = $variation->get_stock_status();
                        $variation->set_stock_status('outofstock');
                        $variation->save();
                        $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' variation removed from feed');
                    }
                    continue;
                }

                if ($variation->get_stock_status() === 'outofstock' && $stockStatus) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('instock');
                    $variation->save();
                    $this->setDelta($product, $old, 'instock', 'status', $variation->get_id() . ' variation status');
                } elseif ($variation->get_stock_status() === 'instock' && !$stockStatus) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('outofstock');
                    $variation->save();
                    $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' variation status');
                }
                if ($variation->get_regular_price() != $price || $variation->get_sale_price() != $salePrice) {
//                    if ($variation->get_sale_price() > 0) {
//                        $variation->set_sale_price('');
//                    }
                    $old = 'regular price:' . $variation->get_regular_price() .' sale price: ' . $variation->get_sale_price();
                    $new = 'regular price:' . $price .' sale price: ' . $salePrice;
                    $variation->set_sale_price($salePrice);
                    $variation->set_regular_price($price);
                    $variation->save();
                    $this->setDelta($product, $old, $new, 'price', $variation->get_id() . ' variation price');
                }
            }
        } else {
            if ($productData->getStatus() === 'publish' && $product->get_status() !== 'publish') {
                $this->setDelta($product, $product->get_status(), 'publish', 'status', 'simple status published');
                $product->set_status('publish');
                $product->save();
            }
            if ($productData->getStatus() === 'draft' && $product->get_status() === 'publish') {
                $this->setDelta($product, $product->get_status(), 'draft', 'status', 'simple status to draft');
                $product->set_status('draft');
                $product->save();
            }
            if ($product->get_stock_status() != $productData->getStockStatus()) {
                $this->setDelta($product, $product->get_stock_status(), $productData->getStockStatus(), 'stockStatus',  'simple stock status');
                $product->set_stock_status($productData->getStockStatus());
                $product->save();
            }
        }
    }
}