<?php

namespace Nss\Feed;
use League\Csv\Reader;
use League\Csv\Writer;

class FeedStatusPage
{
    public $ignoredVendors = [224, 296, 198];


    public function __construct()
    {
        $this->categoryMapper = new CategoryMapper();
    }

    public function getView()
    {
        $ignored = $this->ignoredVendors;
        $redis = new \Redis();
        $redis->connect(REDIS_HOST);

        include(__DIR__ . '/../view/status.php');
    }

}
