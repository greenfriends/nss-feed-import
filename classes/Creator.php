<?php

namespace Nss\Feed;



class Creator
{
    private $imageHandler;

    public function __construct()
    {
        $this->imageHandler = new ImageHandler();
    }

    public function createItem(\WC_Product $product, Product $productData)
    {
        if ($productData->getType() === 'simple') {
            $this->saveSimpleItem($product, $productData);
        } else {
            $this->saveVariableItem($productData);
        }
    }

    private function saveVariableItem(Product $productData)
    {
        try {
            $product = $this->saveMainVariableItem($productData);
            $this->imageHandler->handleImage($productData->getImages(), $product->get_id());
            $product->save();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            //@TODO log..
            $product->set_status('pending');
        }

        //detect product attribute and set variation data
        $variation_data['boja'] = [];
        $variation_data['velicina'] = [];

        foreach ($productData->getOptions() as $option) {
            if (isset($option['boja'])) {
                $variation_data['boja'][] = $option['boja']['value'];
            }
            if (isset($option['velicina'])) {
                $variation_data['velicina'][] = $option['velicina']['value'];
            }
//            var_dump('parsing options');
//            var_dump($option);
        }

        // Iterating through the variations attributes and set attribute values
        foreach ($variation_data as $attribute => $attribute_value) {
            if (!empty($attribute_value)) {
                $attribute = 'pa_' . $attribute;
                foreach ($attribute_value as $name) {
                    $name = sanitize_title($name);
                    wp_set_object_terms($product->get_id(), $name, $attribute, true);
                    $attribute_data[sanitize_title($attribute)] = [
                        'name' => wc_clean($attribute),
                        'value' => $name,
                        'is_visible' => '1',
                        'is_variation' => '1',
                        'is_taxonomy' => '1'
                    ];
//                    var_dump('saving attributes');
//                    var_dump($attribute);
//                    var_dump($attribute_data);
                }
                update_post_meta($product->get_id(), '_product_attributes', $attribute_data);
            }
        }

        //create variation for each attribute value
        foreach ($productData->getOptions() as $option) {
            $variation_post = array(
                'post_title' => $product->get_title(),
                'post_name' => 'product-' . $product->get_id() . '-variation',
                'post_status' => 'publish',
                'post_parent' => $product->get_id(),
                'post_type' => 'product_variation',
                'guid' => $product->get_permalink()
            );
            $variation_post_id = wp_insert_post($variation_post);
            $price = $option['regularPrice'];
            $stockStatus = 'instock';
            if ($option['stockStatus'] !== $stockStatus) {
                $stockStatus = 'outofstock';
            }
            if (isset($option['boja'])) {
                update_post_meta($variation_post_id, 'attribute_pa_boja', sanitize_title($option['boja']['value']));
            }
            if (isset($option['velicina'])) {
                $save = update_post_meta($variation_post_id, 'attribute_pa_velicina', sanitize_title($option['velicina']['value']));
//                var_dump($save);
//                var_dump($variation_post_id);
            }

            $variation = wc_get_product($variation_post_id);
            $variation->set_stock_status($stockStatus);
            $variation->set_regular_price($price);
            $variation->set_price($price);
            $variation->set_weight($option['weight']);
            $save = $variation->save();
//            var_dump($save);
//            var_dump('saving variations');
        }
    }

    private function createProductAttribute($productId, $attribute, $attributeValue)
    {
//        $product->validate_props(); // Check validation

        $taxonomy = sanitize_title('pa_' . $attribute);
        // If taxonomy doesn't exists we create it (Thanks to Carl F. Corneil)
        if(!taxonomy_exists($taxonomy)) {
            $save = register_taxonomy(
                $taxonomy,
                'product_variation',
                array(
                    'hierarchical' => false,
                    'label' => ucfirst($attribute),
                    'query_var' => true,
                    'rewrite' => array('slug' => sanitize_title($attribute)), // The base slug
                )
            );
            var_dump($save);
            echo 'registered taxonomy:' . $taxonomy . PHP_EOL;
        }
        // Check if the Term name exist and if not we create it.
        if (!term_exists($attributeValue, $taxonomy)) {
            echo 'created term:' . $attributeValue . PHP_EOL;
            wp_insert_term($attributeValue, $taxonomy); // Create the term
        }

        $term_slug = get_term_by('name', $attributeValue, $taxonomy )->slug; // Get the term slug
        // Get the post Terms names from the parent variable product.
        $post_term_names =  wp_get_object_terms($productId, $taxonomy, array('fields' => 'names'));

        // Check if the post term exist and if not we set it in the parent variable product.
        if(!in_array($attributeValue, $post_term_names)) {
            wp_set_object_terms($productId, $attributeValue, $taxonomy, true);
        }

//        $post_term_names =  wp_get_object_terms($productId, $taxonomy, array('fields' => 'names'));
//        var_dump($post_term_names);
//        die();

        // Set/save the attribute data in the product variation
        update_post_meta($productId, 'attribute_' . $taxonomy, $term_slug);

//        $attributeValue = sanitize_title($attributeValue);
//        wp_set_object_terms($productId, $attributeValue, $attribute, true);
//        update_post_meta($productId, '_product_attributes', [$attribute => [
//            'name' => wc_clean($attribute),
//            'value' => $attributeValue,
//            'is_visible' => '1',
//            'is_variation' => '1',
//            'is_taxonomy' => '1'
//        ]]);
    }

    private function saveMainVariableItem(Product $productData)
    {
//set basic data for parent product
        $post = array(
            'post_content' => $productData->getDescription(),
            'post_status' => $productData->getStatus(),
            'post_title' => $productData->getName(),
            'post_type' => 'product'
        );
        $post_id = wp_insert_post($post);
        wp_set_object_terms($post_id, 'variable', 'product_type', true);
        $product = wc_get_product($post_id);
        $product->set_status($productData->getStatus());
        $price = $productData->getRegularPrice();
        if ($productData->getSalePrice() > 0) {
            $product->set_sale_price($productData->getSalePrice());
            $price = $productData->getSalePrice();
        }
        if ($productData->getInputPrice() > 0) {
            $product->update_meta_data('input_price', $productData->getInputPrice());
        }
        $product->set_catalog_visibility('visible');
        $product->set_short_description($productData->getShortDescription());
        $product->set_stock_status($productData->getStockStatus());
        $product->set_weight($productData->getWeight());
        $product->set_reviews_allowed(1);
        $product->set_price($price);
        $product->set_regular_price($productData->getRegularPrice());
        $product->update_meta_data('pa_proizvodjac', $productData->getManufacturer());
        $product->update_meta_data('pdv', $productData->getPdv());
        $product->update_meta_data('vendor_code', $productData->getSupplierSku());
        $product->update_meta_data('supplier', $productData->getSupplierId());
        $product->update_meta_data('quantity', 0);
        $categories = explode(',', $productData->getCategoryIds());
        if (count($categories) === 0) {
            var_dump('no categories' . $productData->getCategoryIds());
            die();
            throw new \Exception('no categories ' . $productData->getCategoryIds());
        }
        $product->set_category_ids($categories);
        $product->save();
        delete_transient( 'wc_product_children_' . $post_id );
        delete_transient( 'wc_var_prices_' . $post_id );
        $product->set_sku($product->get_id());

        return $product;
    }

    /**
     * Save product data when item is inserted into database.
     *
     * @TODO sku
     *
     * @param \WC_Product $product
     * @param Product $productData
     *
     * @throws \Exception if ...
     *
     * @return \WC_Product
     */
    private function saveSimpleItem(\WC_Product $product, Product $productData)
    {
        $product->set_name($productData->getName());
        $product->set_status($productData->getStatus());
        $price = $productData->getRegularPrice();
        if ($productData->getSalePrice() > 0) {
            $price = $productData->getSalePrice();
            $product->set_sale_price($productData->getSalePrice());
        }
        if ($productData->getInputPrice() > 0) {
            $product->update_meta_data('input_price', $productData->getInputPrice());
        }
        $product->set_catalog_visibility('visible');
        $product->set_short_description($productData->getShortDescription());
        $product->set_description($productData->getDescription());
        $product->set_stock_status($productData->getStockStatus());
        $product->set_weight($productData->getWeight());
        $product->set_reviews_allowed(1);
        $product->set_price($price);
        $product->set_regular_price($productData->getRegularPrice());
        $product->update_meta_data('pa_proizvodjac', $productData->getManufacturer());
        $product->update_meta_data('pdv', $productData->getPdv());
        $product->update_meta_data('vendor_code', $productData->getSupplierSku());
        $product->update_meta_data('supplier', $productData->getSupplierId());
        $product->update_meta_data('quantity', $productData->getQuantity());
        if (!is_array($productData->getCategoryIds())) {
            $categories = explode(',', $productData->getCategoryIds());
        } else {
            $categories = $productData->getCategoryIds();
        }
        if (count($categories) === 0) {
            $product->delete(true);
            throw new \Exception('no categories for: ' . $productData->getName());
        }
        $product->set_category_ids($categories);
        $product->save();

        $product->set_sku($product->get_id());
        try {
            $saveImage = $this->imageHandler->handleImage($productData->getImages(), $product->get_id());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            //@TODO log..
            $product->set_status('pending');
        }
        $product->save();

        return $product;
    }
}