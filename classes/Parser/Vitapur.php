<?php

namespace Nss\Feed\Parser;

use Nss\Feed\Product;

class Vitapur 
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:vitapur:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:vitapur:';

    const SUPPLIER_ID = 268;

    private $httpClient;

    private $redis;

    private $source = 'https://www.vitapur.si/media/feed/non-stop-shop-rs.xml';

    private $errors = [];

    public function __construct(\GuzzleHttp\Client $client, \Redis $redis)
    {
        $this->httpClient = $client;
        $this->redis = $redis;
    }

    public function getXml()
    {
        try {
            $response = $this->httpClient->get($this->source);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        $xml = $response->getBody()->getContents();

        return simplexml_load_string($xml, null, LIBXML_NOCDATA);
    }

    public function processItems()
    {
        global $wpdb;
        $existingItems = [];
        $newItems = [];
        $totalParsed = 0;
        $i = 0;
        $products = $this->getXml();
        foreach ($products as $product) {
            $i++;
            $vendorCode = (string) $product->sku;
            $sql = "SELECT post_id FROM wp_postmeta WHERE meta_key  = 'vendor_code' AND meta_value = '{$vendorCode}'";
            $result = $wpdb->get_results($sql);
            try {
                if (!empty($result)) {
                    $product = $this->parseSource($product, $result[0]->post_id);
                    $existingItems[] = $result[0]->post_id;
                    $cacheKey = self::CACHE_KEY_UPDATE;
                } else {
                    $product = $this->parseSource($product);
                    $newItems[] = $vendorCode;
                    $cacheKey = self::CACHE_KEY_CREATE;
                }
            } catch (\Exception $e) {
                $this->errors[$vendorCode] = $e->getMessage();
                continue;
            }

            $serializedProduct = serialize($product);
            $key = md5($product->getName() . $product->getRegularPrice() . $product->getImages());
            $this->redis->set($cacheKey . $key, $serializedProduct);
            $this->redis->sAdd($cacheKey . 'index', $key);
            $totalParsed++;
        }

        return [
            'total' => count($products),
            'parsed' => $totalParsed,
            'existing' => count($existingItems),
            'new' => count($newItems),
            'errors' => $this->errorCount(),
        ];
    }

    public function errorCount()
    {
        return count($this->errors);
    }

    public function parseErrors()
    {
        $msg = '';
        if ($this->errorCount()) {
            $msg = sprintf('There was a total of %s errors.', count($this->errors)) . PHP_EOL;
        }
        foreach ($this->errors as $error) {
            $msg .= $error . PHP_EOL;
        }

        return $msg;
    }

    function parseSource($product, $postId = false)
    {
        $name = trim((string) $product->naziv);
        $description = (string) $product->opis;
        $shortdesc = (string) $product->kratak_opis;
//        $status = 'pending'; // pending
//        if((int) $product->dostupnost === 1) {
        $status = 'publish';
        $stock_status = 'instock';
//        }

//        if((int) $product->dostupnost === 0) {
//            $stock_status = 'outofstock';
//        }
        $type = 'simple';

        $catsData = explode("\n", file_get_contents(__DIR__ . '/vitapur.cats.csv'));
        $categories = $this->getCategories((string) $product->kategorija, $catsData);

        $dto = [
            'sku' => (string) $product->sku,
            'postId' => $postId,
            'supplierSku' => (string) $product->sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => implode(',', $categories),
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'imageIds' => '',
            'images' => (string) $product->slika,
            'regularPrice' => $this->parsePrice($product->mp_cena),
            'salePrice' => $this->parsePrice($product->akcijska_cena),
            'inputPrice' => $this->parsePrice($product->vp_cena),
            'stockStatus' => $stock_status,
            'pdv' => '20',
            'boja' => '',
            'type' => $type,
            'velicina' => '',
            'weight' => (string) $product->tezina,
            'quantity' => '0',
            'manufacturer' => ''
        ];

        return new Product($dto);
    }

    private function getCategories($catName, $catsData)
    {
        $categories = [];
        foreach ($catsData as $catDatum) {

            $catDatum = explode(',', $catDatum);
//            var_dump(trim($catDatum[0]));
//            var_dump(trim($catDatum[1]));
//            die();
//            if (strtolower($catDatum[0]) === strtolower($catName)) {
            if ($catDatum[0] === $catName) {
                $catId = trim($catDatum[1]);

                //skip not defined categories
                if ($catId === '') {
                    var_dump($catDatum);
                    continue;
                }

                if ($catId == 0) {
                    throw new \Exception(sprintf('Skipping - category %s does not have mapping defined.', $catName));
                }

                $cat = get_term($catId, 'product_cat');
                if (get_class($cat) !== \WP_Term::class) {
                    if (get_class($cat) === \WP_Error::class) {
                        foreach ($cat->errors as $code => $error) {
                            $this->errors[] = $error[0] . ' for category id ' . $catId;
                            continue;
                        }
//                        var_dump($cat->errors);
//                        die();
//                        $this->errors[] = $cat->errors[0] . ' for category id ' . $catId;
                        continue;
                    }
                }
                $categories = [$cat->term_id];
                if ($cat->parent) {
                    $parent = get_term($cat->parent, 'product_cat');
                    $categories[] = $parent->term_id;

                    if ($parent->parent) {
                        $grandParent = get_term($parent->parent, 'product_cat');
                        $categories[] = $grandParent->term_id;
                    }
                }
            }
        }
//        if (count($categories) === 0) {
//            var_dump($catName);
//            die();
//            throw new \Exception(sprintf('Category %s does not have mapping defined.', $catName));
//        }

        return $categories;
    }

    private function parsePrice($price)
    {
        $parsedPrice = (string) $price;

        $parsedPrice = str_replace('.00', '', $parsedPrice);
        $parsedPrice = str_replace(',','', $parsedPrice);

        return  $parsedPrice;
    }

}