<?php

namespace Nss\Feed\Parser;

use Nss\Feed\Product;

class Asport extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:asport:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:asport:';

    const SUPPLIER_ID = 252;

    protected $source = 'ftp://178.222.248.231/NonStopShop.xml';

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = $this->cleanUpString($product->description);
        $shortdesc = $this->cleanUpString($product->shortdesc);
        $name = $this->cleanUpString($product->name);

        $status = 'pending';
        if((int) $product->online === 1) {
            $status = 'publish';
        }
        $stock_status = 'instock';
        if($product->unavailable = 0) {
            $stock_status = 'outofstock';
        }
        $sizes = $this->parseSizes($product->sizes);
        $type = 'simple';
        if (count($sizes['boja']) || count($sizes['velicina'])) {
            $type = 'variable';
        }
        $catsData = explode("\n", file_get_contents(__DIR__ . '/../../old.cats.map.csv'));
        $categories = $this->getCategories((int) $product->categoryid, $catsData);
        if (count($categories) === 0) {
            throw new \Exception(sprintf('Item %s has non existent categoryId: %s.', $name, (int) $product->categoryid));
        }
        $salePrice = 0;
        $regularPrice = (int) $product->baseprice;
        if ((int) $product->promoprice > 0 && (int) $product->promoprice != (int) $product->baseprice) {
            $salePrice = (int) $product->baseprice;
            $regularPrice = (int) $product->promoprice;
        }

        $dto = [
            'sku' => '',
            'postId' => $postId,
            'supplierSku' => (string) $product->vendorcode,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => implode(',', $categories),
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'imageIds' => '',
            'images' => (string) $product->image,
            'regularPrice' => $this->parsePrice($regularPrice),
            'salePrice' => $this->parsePrice($salePrice),
            'inputPrice' => $this->parsePrice($product->inputprice),
            'stockStatus' => $stock_status,
            'pdv' => (int) $product->pdv,
            'postPaid' => (int) $product->postpaid,
            'manufacturer' => (string) $product->manufacturer,
            'boja' => $sizes['boja'],
            'type' => $type,
            'velicina' => $sizes['velicina'],
            'weight' => 1,
            'quantity' => ''
        ];

        return new Product($dto);
    }

    private function getCategories($id, $cats) {
        $categories = array();
        foreach ($cats as $catDataString) {
            $catData = str_getcsv($catDataString, ",", '"');
            if ($catData[0] === '') {
                continue;
            }
            if (isset($catData[3])) {
                if (in_array($id, explode(',', $catData[3]))) {
                    $cat = get_term_by('name', trim($catData[0]), 'product_cat');
                    $categories[] = $cat->term_id;
                    if (isset($catData[1])) {
                        $name = trim($catData[1]);
                        $cat = get_term_by('name', $name, 'product_cat');
                        $categories[] = $cat->term_id;
                    }
                    if (isset($catData[2]) && $catData[2] != '') {
                        $name = trim($catData[2]);
                        $cat = get_term_by('name', $name, 'product_cat');
                        if (!is_object($cat)) {
                            continue;
                        }
                        $categories[] = $cat->term_id;
                    }
                }
            }
        }

        return $categories;
    }

    protected function fetchItems()
    {
        $localFile = ABSPATH . 'wp-content/uploads/feed/asport.xml';
        $urlInfo = parse_url($this->source);
        $ftpConnection = ftp_connect($urlInfo['host']);
        ftp_login($ftpConnection, 'anonymous', 'anonymous@example.org');
        ftp_pasv($ftpConnection, true);
        if (!ftp_get($ftpConnection, $localFile, $urlInfo['path'], FTP_BINARY)) {
            var_dump(error_get_last());
            throw new \Exception(sprintf('Could not fetch feed %s from %s. ', $urlInfo['path'], $urlInfo['host']));
        }
        ftp_close($ftpConnection);

        $this->products = simplexml_load_file($localFile, null, LIBXML_NOCDATA)->product;
    }

    private function parseSizes(\SimpleXMLElement $sizes)
    {
        $sizeAttributes = $sizes->attributes();
        $gtype = (int) $sizeAttributes[0];
        if (!$gtype) {
            throw new \Exception('Invalid size type provided.');
        }

        $returnSizes = [
            'velicina' => [],
            'boja' => []
        ];
        foreach ($sizes->children() as $size) {
            $sizeStatus = $size->attributes();
            $status = (int) $sizeStatus[0];
            if ($status) {
                if ($gtype === 1) {
                    $returnSizes['velicina'][] = (string) $size;
                } else {
                    $returnSizes['boja'][] = (string) $size;
                }
            }
        }

        return $returnSizes;
    }

    private function parsePrice($price)
    {
        return (int) $price;
    }

    /**
     * How do you create xml like that ffs :S
     *
     * @param $string
     * @return mixed|string
     */
    private function cleanUpString($string)
    {
        $newString = str_replace('![CDATA[', '', trim((string) $string));
        $newString = str_replace(']]', '', trim((string) $newString));
        $newString = str_replace('   ', ' ', trim((string) $newString));
        $newString = str_replace('  ', ' ', trim((string) $newString));
        $newString = html_entity_decode($newString);

        return $newString;
    }
}