<?php

namespace Nss\Feed\Parser;

use Nss\Feed\Product;

class Sense extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:sense:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:sense:';
    const SUPPLIER_ID = 198;

    protected $source = 'https://portal.wings.rs/api/v1/sense/open_gate?handle=export_nonstopshop';

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) trim($product->description);
        $shortdesc = (string) trim($product->shortdesc);
        $name = (string) trim($product->name);

        $status = 'pending'; // draft
        if((int) $product->online === 1) {
            $status = 'publish';
        }
        $stock_status = 'instock';
        if($product->unavailable = 0) {
            $stock_status = 'outofstock';
        }
        $type = 'simple';
        $catsData = explode("\n", file_get_contents(__DIR__ . '/../../old.cats.map.csv'));
        $categories = $this->getCategories((int) $product->categoryid, $catsData);
        if (count($categories) === 0) {
            throw new \Exception(sprintf('Item %s has non existent categoryId: %s.', $name, (int) $product->categoryid));
        }
        $regularPrice = (int) $product->baseprice;
        if ((int) $product->promoprice > 0 && (int) $product->promoprice < (int) $product->baseprice) {
            $regularPrice = (int) $product->promoprice;
        }

        $dto = [
            'sku' => (string) $product->vendorcode,
            'postId' => $postId,
            'supplierSku' => (string) $product->vendorcode,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => implode(',', $categories),
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'imageIds' => '',
            'images' => (string) $product->image,
            'regularPrice' => $this->parsePrice($regularPrice),
            'salePrice' => '',
            'inputPrice' => $this->parsePrice($product->inputprice),
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => (int) $product->postpaid,
            'manufacturer' => (string) $product->manufacturer,
            'boja' => [],
            'type' => $type,
            'velicina' => [],
            'weight' => 0.5, // @TODO check weight for imported items
            'quantity' => ''
        ];

        return new Product($dto);
    }

    function getCategories($id, $cats) {
        $categories = array();
        foreach ($cats as $catDataString) {
            $catData = str_getcsv($catDataString, ",", '"');
            if ($catData[0] === '') {
                continue;
            }
            if (isset($catData[3])) {
                if (in_array($id, explode(',', $catData[3]))) {
                    $cat = get_term_by('name', trim($catData[0]), 'product_cat');
                    $categories[] = $cat->term_id;
                    if (isset($catData[1])) {
                        $name = trim($catData[1]);
                        $cat = get_term_by('name', $name, 'product_cat');
                        $categories[] = $cat->term_id;
                    }
                    if (isset($catData[2]) && $catData[2] != '') {
                        $name = trim($catData[2]);
                        $cat = get_term_by('name', $name, 'product_cat');
                        if (!is_object($cat)) {
                            $this->errors[] = 'category is missing: ' . $name;
                            die();
                        }
                        $categories[] = $cat->term_id;
                    }
                }
            }
        }

        return $categories;
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
//        return simplexml_load_file(__DIR__ . '/sense.xml', null, LIBXML_NOCDATA);
        try {
            $response = $this->getHtpClient()->get($this->source);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        $result = $response->getBody()->getContents();
//        file_put_contents(ABSPATH . 'wp-content/uploads/sense.xml', $result);

        $this->products = simplexml_load_string($result, null, LIBXML_NOCDATA)->product;
    }

    private function parsePrice($price)
    {
        return (int) $price;
    }
}