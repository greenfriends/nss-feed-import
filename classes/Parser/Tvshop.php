<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Tvshop extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:tvshop:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:tvshop:';
    const SUPPLIER_ID = 123;

    protected $source = 'https://www.tv-shop.tv/non-stop-shop-cenovnik.xml';

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->description;
        $shortdesc = (string) $product->shortdesc;
        $name = (string) $product->name;

        $stock_status = 'instock';
        if((int) $product->stockStatus === 0) {
            $stock_status = 'outofstock';
        }
        $options = [];
        $productType = (string) $product->type;
        $salePrice = '';
        $regularPrice = '';
        if ($productType === 'variable') {
            $stock_status = 'instock';
            $sizeType = (int) $product->sizes->attributes()[0];
            if (!$sizeType) {
                throw new \Exception('Invalid size type provided.');
            }

            $images = [];
            foreach ($product->sizes->children() as $size) {
                $status = ((int) $size->online) ? 'publish': 'draft';

                $regularPrice = (int) $size->baseprice;
                if ((int) $size->promoprice > 0 && (int) $size->promoprice < (int) $size->baseprice) {
                    $regularPrice = (int) $size->promoprice;
                }
                if ($regularPrice == '0.0000') {
                    $regularPrice = 0;
                }
                if (!in_array((string) $size->image, $images)) {
                    $images[] = (string) $size->image;
                }
                $value = trim(str_replace($name, '', (string) $size->value));
                $stockStatus = ((int) $size->online && (bool) (string) $size->stockStatus);
                //append vendorcode if option value same as product name, THE FUCK !?
                if ($value == '') {
                    $value = (string) $product->vendorcode;
                }
                if ($sizeType === 1) {
                    $type = 'velicina';
                } else {
                    $type = 'boja';
                }
                $options[] = [
                    'stockStatus' => $stockStatus,
                    'regularPrice' => $regularPrice,
                    'salePrice' => '',
                    'weight' => 0.1,
                    $type => [
                        'value' => $value
                    ]
                ];
            }
        } else {
            $images[] = (string) $product->image;
            $salePrice = (int) $product->promoprice;
            $regularPrice = (int) $product->baseprice;
            if ((int) $product->promoprice > 0) {
                $salePrice = (int) $product->baseprice;
                $regularPrice = (int) $product->promoprice;
            }
            if ((string) $product->promoprice === '0.0000') {
                $salePrice = '';
            }
            if((int) $product->online === 1) {
                $status = 'publish';
            } else {
                $status = 'draft';
            }
        }

        $categoryIds = [(int) $product->categoryid];
        $cat3 = get_term_by('id', (int) $product->categoryid, 'product_cat');
        if ($cat3->parent) {
            $categoryIds[] = $cat3->parent;
            $cat2 = get_term_by('id', $cat3->parent, 'product_cat');
            if ($cat2->parent) {
                $categoryIds[] = $cat2->parent;
            }
        }

        $dto = [
            'sku' => '',
            'postId' => $postId,
            'supplierSku' => (string) $product->vendorcode,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => implode(",", $categoryIds),
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'images' => implode(',', $images),
            'regularPrice' => $regularPrice,
            'salePrice' => $salePrice,
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'pdv' => (int) $product->pdv,
            'postPaid' => (int) $product->postpaid,
            'manufacturer' => (string) $product->manufacturer,
            'boja' => '',
            'type' => $productType,
            'velicina' => '',
            'options' => $options,
            'weight' => 0.1,
            'quantity' => 0
        ];

        return new Product($dto);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $response = $this->getHtpClient()->send(new Request('get', $this->source));
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->Product;
    }
}