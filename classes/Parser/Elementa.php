<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Elementa extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:elementa:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:elementa:';
    const SUPPLIER_ID = 39;

    protected $source = 'https://www.elementa.rs/index/product-list-xml-dkwidlvehmf';
    protected $sourceCategoriesUrl = 'https://www.elementa.rs/index/category-list-xml';

    protected $catLog = [];

    protected $sourceCategories;

    public function __construct(\GuzzleHttp\Client $client, \Redis $redis)
    {
        parent::__construct($client, $redis);
    }

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->opis;
        $shortdesc = (string) $product->opis;
        $name = (string) $product->naziv;

        $stock_status = 'instock';
        $status = 'publish';
        if((int) $product->lagerVp < 5) {
            $stock_status = 'outofstock';
        }
        $options = [];
        $type = 'simple';
        $images = (string) $product->slika;
        $regularPrice = ceil((string) $product->cena);
        if ($regularPrice == 0) {
            $status = 'draft';
        }

        $this->parseCategories((int) $product->parent, $product);
//        die();

//        $categories = $this->parseCategories((string) $product->kategorija);

        $dto = [
            'sku' => (string) $product->textId,
            'postId' => $postId,
            'supplierSku' => (string) $product->textId,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => '3152',
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'images' => $images,
            'regularPrice' => $regularPrice,
            'salePrice' => '',
            'inputPrice' => (string) $product->vpCena,
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => '',
            'boja' => '',
            'type' => $type,
            'options' => $options,
            'velicina' => '',
            'weight' => (string) $product->tezina,
            'quantity' => 0
        ];

        return new Product($dto);
    }

    private function parseCategories($sourceCatId, $product)
    {
        $catId = null;
        if (!isset($this->sourceCategories[$sourceCatId])) {
            throw new \Exception('No category exists for this item.');
        }
        $lastLvlCat = $this->sourceCategories[$sourceCatId];
        if ($lastLvlCat['NadredjenaKatID'] > 0) {
            $midLvlCat = $this->sourceCategories[$lastLvlCat['NadredjenaKatID']];
        }
        if ($midLvlCat['NadredjenaKatID'] > 0) {
            $topLvlCat = $this->sourceCategories[$midLvlCat['NadredjenaKatID']];
        }

        $catString = '"' . $midLvlCat['Naziv'] .'","'. $lastLvlCat['Naziv'] .'"';
        if (isset($topLvlCat)) {
            $catString = $topLvlCat['Naziv'] .','. $catString;
        }
        if (!in_array($catString, $this->catLog)) {
            $this->catLog[] = $catString;
            echo $catString . PHP_EOL;
        }
        return;

//        var_dump($lastLvlCat['KatID']);
//        var_dump($midLvlCat['KatID']);
//        var_dump($topLvlCat['KatID']);
//        die();

        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $response = $this->getHtpClient()->send(new Request('get', $this->source));
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->xpath('//product');

        $response = $this->getHtpClient()->send(new Request('get', $this->sourceCategoriesUrl));
        $data = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->xpath('//Kat');

        $categories = [];
        foreach ($data as $item) {
            $item = (array) $item;
            if ($item['@attributes']['Naziv'] === '.') {
                continue;
            }
            $categories[$item['@attributes']['KatID']] = $item['@attributes'];
//            var_dump($categories);
//            die();
        }
        $this->sourceCategories = $categories;
//        var_dump($this->sourceCategories[1011]);
//        die();
    }
}