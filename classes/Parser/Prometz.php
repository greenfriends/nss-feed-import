<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Prometz extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:prometz:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:prometz:';
    const SUPPLIER_ID = 230932;

    protected $source = 'https://www.prometz.rs/xml/nonstopshop.xml';
    protected $useMapping = true;

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->opis;
        $shortdesc = (string) $product->kratak_opis;
        $name = (string) $product->naziv;

        $stock_status = 'instock';
        $status = 'publish';
        if((int) $product->status !== 1) {
            $stock_status = 'outofstock';
        }
        //ignored items
        if ((string) $product->sifra === '00262') {
            $stock_status = 'outofstock';
        }

        $boja = '';
        $velicina = '';
        $options = [];
        $type = 'simple';
        $images = (string) $product->slike;
        $regularPrice = str_replace(',', '.', str_replace('.', '', (string) $product->cena));
        $salePrice = str_replace(',', '.', str_replace('.', '', (string) $product->cena_sa_popustom));
        $regularPrice = number_format($regularPrice, 0, '', '');
        $salePrice = number_format($salePrice, 0, '', '');
        if ($salePrice == 0) {
            $salePrice = '';
            var_dump($name);
            die();
        }
//        if ($salePrice > 0 && $salePrice < $regularPrice) {
//            $regularPrice = $salePrice;
//        }

        $varijacije = (array) $product->varijacije;
        if (!empty($varijacije)) {
            $type = 'variable';
            if (isset($varijacije['varijacija'][0]->osobine->osobina)) {
                $i = 0;
                foreach ($varijacije['varijacija'] as $varijacija) {
                    if (!isset($varijacija->osobine->osobina->tip)) {
                        throw new \Exception('Invalid varijacija attribute format. ' . $product->sifra);
                    }
                    if ((string) $varijacija->osobine->osobina->tip === 'pattern') {
                        throw new \Exception('Skipping invalid item value - url. ' . $product->sifra);
                    }
                    if ((string) $varijacija->osobine->osobina->vrednost === '-') {
                        throw new \Exception('Skipping invalid item value.');
                    }
                    $options[$i]['stockStatus'] = (int) $varijacija->status;
                    $options[$i]['regularPrice'] = $regularPrice;
                    $options[$i]['salePrice'] = $salePrice;

                    foreach ($varijacija->osobine->osobina as $osobina) {
//                        var_dump($osobina);
                        if ((string) $osobina->tip === 'size') {
                            $options[$i]['velicina'] = [
                                'value' => (string) $osobina->vrednost,
                            ];
                        }
                        if ((string) $osobina->tip === 'color') {
                            $options[$i]['boja'] = [
                                'value' => (string) $osobina->vrednost,
                            ];
                        }

                    }
                    $i++;
//                    die();


//                    var_dump((string)$varijacija->osobine->osobina->tip);
//                    var_dump((string)$varijacija->osobine->osobina->vrednost);
                }
//                $boja = $returnSizes['boja'];
//                $velicina = $returnSizes['velicina'];
            } else {
                throw new \Exception('Invalid size param. ' . $product->sifra);
            }
        }

        if (in_array($postId, [523449, 523321])) {
            $status = 'draft';
        }

        $catString = (string) $product->kategorija;
        if (!in_array($catString, $this->sourceCategories)) {
            $this->sourceCategories[] = $catString;
        }

        $categories = $this->parseCategories($catString);

        $dto = [
            'sku' => '',
            'postId' => $postId,
            'supplierSku' => (string) $product->sifra,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $categories,
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'images' => $images,
            'regularPrice' => $regularPrice,
            'salePrice' => $salePrice,
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => '',
            'boja' => $boja,
            'type' => $type,
            'options' => $options,
            'velicina' => $velicina,
            'weight' => (string) $product->tezina,
            'quantity' => 0
        ];

        return new Product($dto);
    }

    private function parseCategories($cats)
    {
        $catId = null;
        foreach ($this->mappedCategories->getIterator() as $row => $item) {
            if ($row === 0) {
                continue;
            }
            if ($item['source1'] === $cats) {
                $catId = $item['localId1'];
                break;
            }
        }

        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return void
     */
    protected function fetchItems()
    {
        $response = $this->getHtpClient()->send(new Request('get', $this->source));
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->xpath('//Proizvod');
//        $this->products = simplexml_load_string(file_get_contents(__DIR__ . '/prometz-test.xml'), null, LIBXML_NOCDATA)->xpath('//Proizvod');
    }
}