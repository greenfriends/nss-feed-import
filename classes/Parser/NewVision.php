<?php


namespace Nss\Feed\Parser;

use Nss\Feed\Product;

class NewVision extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:newvision:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:newvision:';
    const SUPPLIER_ID = 193;

    protected $useMapping = true;
    protected $source = 'https://www.love4yu.com/wp-content/uploads/woo-product-feed-pro/csv/QIMOcGrW8QoXespBDn1dPN0f7e1dgjRX.csv';

    protected function fetchItems()
    {
        $response = $this->getHtpClient()->get($this->source);
        $this->products = $this->getFormatedProducts($response);
    }

    protected function parseSource($product, $id = null)
    {
        $status = 'publish';
        $stockStatus = 'instock';
        $type = 'simple';
        if($product['stockStatus'] !== 'in stock') {
            $stockStatus = 'outofstock';
        }
//        $categoryIds = $this->parseCategories($product['mainCat'], $product['cats']);

        $explodedCats = explode('||', ltrim(htmlspecialchars_decode($product['cats']),'||'));
        $cats = [];
        $cats[0] = $explodedCats[0];
        unset($explodedCats[0]);
        $cats[1] = implode('||', $explodedCats);
        $formattedCats = $cats;
        if (!strlen($cats[1])) {
            unset($cats[1]);
        }
        $catString = implode('###', $cats);
        if (!in_array($catString, $this->sourceCategories)) {
            $this->sourceCategories[] = $catString;
        }
        $categoryIds = $this->parseCategories($formattedCats);

        $imageUrl = $product['image'] .','. $product['image2'];
        $description = $product['title'] .PHP_EOL. $product['description'];
//        var_dump($product['id']);
//        var_dump($product['title']);
//        die();

        $dto = [
            'sku' => '',
            'postId' => $id,
            'supplierSku' => $product['id'],
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $categoryIds,
            'name' => $product['title'],
            'status' => $status,
            'shortDescription' => '',
            'description' => $description,
            'images' => $imageUrl,
            'regularPrice' => $product['price'] + $product['price'] * 0.20,
            'salePrice' => '',
            'inputPrice' => $product['price'],
            'stockStatus' => $stockStatus,
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => '',
            'boja' => '',
            'type' => $type,
            'velicina' => '',
            'options' => [],
            'weight' => 0.1,
            'quantity' => 0
        ];
        return new Product($dto);
    }

    private function parseCategories($cats)
    {
        $catId = null;
        foreach ($this->mappedCategories->getIterator() as $row => $item) {
            if ($row === 0) {
                continue;
            }
            if ((int) $item['localId1'] === 0) {
                continue;
            }

            if ($item['source1'] !== $cats[0]) {
                continue;
            }
            if (isset($cats[1]) && $cats[1] === $item['source2']) {
                $catId = $item['localId1'];
                $catObject = get_term_by('id', $catId, 'product_cat');
                if (!$catObject) {
                    var_dump($cats);
                    var_dump($item);
                    die('prvi');
                }
                if ($catObject->parent !== 2690) {
                    $parent = get_term_by('id', $catObject->parent, 'product_cat');
                    $catId = $parent->term_id .','. $catObject->term_id;
                }
            }
        }
        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    private function getCategoryLevel($cat_id) {
        $cat = get_term_by('id', $cat_id, 'product_cat');
        if ($cat->parent === 0){
            return 1;
        }
        if (get_term($cat->parent, 'product_cat')->parent === 0){
            return 2;
        }
        return 3;
    }

    protected function getCatTree($catId)
    {
        return '2690,' . $catId;
    }

    private function getFormatedProducts($response)
    {
        $data = str_getcsv($response->getBody()->getContents(),"\n");
        $items = [];
        foreach($data as $key => $row) {
            if ($key === 0) {
                continue;
            }
            $row = str_getcsv($row, ",");
            $items[] = [
                'id' => $row[0],
                'title' => $row[1],
                'description' => $row[2],
                'url' => $row[3],
                'image' => $row[4],
                'price' => str_replace(' RSD', '', $row[5]),
                'cats' => $row[6],
                'stockStatus' => $row[7],
                'image2' => $row[8],
            ];
        }
        return $items;
    }
}