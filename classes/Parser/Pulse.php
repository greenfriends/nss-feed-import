<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Pulse extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:pulse:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:pulse:';
    const SUPPLIER_ID = 296;

    private $catLog = [];

    protected $source = 'https://pulseserbia.com/rest-api/get-products.php?limit=%s&offset=%d';

    protected $loginUrl = 'https://pulseserbia.com/rest-api/login.php';

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = $product->opis;
        $name = $product->naziv;
        $sku = $product->code;
        if (strlen($sku) === 0) {
            throw new \Exception('No sku set.');
        }
        $stock_status = 'instock';
        if((int) $product->stanje_lagera < 5) {
            $stock_status = 'outofstock';
        }
        $type = 'simple';
        $images[] = $product->images;

        $regularPrice = number_format((int) $product->cena, 0, ',', '');

        //status is only imported first time
        $status = 'publish';
        if ($regularPrice == 0) {
            throw new \Exception('No price set.');
            $status = 'draft';
        }

        $catId = explode(',', $product->ids_kategorije)[0];
        $catName = explode(',', $product->kategorija)[0];

        //category mapping template
//        if (!in_array((int) $catId, $this->catLog)) {
//            $this->catLog[] = (int) $catId;
//            echo $catId .','. $catName . PHP_EOL;
//        }

        $categories = $this->parseCategories($catId);
        $p = new Product([
            'sku' => $sku,
            'postId' => $postId,
            'supplierSku' => $sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $categories,
//            'categoryIds' => [],
            'name' => $name,
            'status' => $status,
            'shortDescription' => '',
            'description' => $description,
            'images' => implode(',', str_replace("'", '', $images)),
            'regularPrice' => $regularPrice,
            'salePrice' => '',
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => 1,
            'manufacturer' => '',
            'boja' => [],
            'type' => $type,
            'velicina' => [],
            'weight' => 0.1,
            'quantity' => 0
        ]);

        return $p;
    }

    private function parseCategories($cats)
    {
        return $this->fetchCategoryMapping($cats);
    }

    private function fetchCategoryMapping($cats)
    {
        $row = 0;
        $catId = null;
        //@TODO optimize, fetch data once
        $handle = fopen(__DIR__ . "/../../mapping/pulse.csv", "rb");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $row++;
            if ($row === 1) {
                continue;
            }

            if ($cats === $data[0]) {
                $catId = $data[2];
                break;
            }
        }
        fclose($handle);

        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $user = 'nonstopshop';
        $pass = 'nonstopshoppulse';
        $loginData = [
            'username' => $user,
            'password' => $pass
        ];
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $response = $this->getHtpClient()->send(new Request('post', $this->loginUrl, $headers, json_encode($loginData)));
        if ($response->getStatusCode() !== 200) {
            var_dump($response->getBody());
            die('no res');
        }

        $jwtData = json_encode(['jwt' => json_decode($response->getBody())->jwt]);
        $perPage = 10;
        $page = 1;
        $count = 1;
        $items = [];
        while ($count !== 0) {
            $response = $this->getHtpClient()->send(new Request('post', sprintf($this->source, $perPage, $perPage * $page), $headers, $jwtData));
            $newItems = json_decode($response->getBody()->getContents())->products;
            $items = array_merge($items, $newItems);
            $count = count($newItems);
            $page++;
        }
        $this->products = $items;
    }
}