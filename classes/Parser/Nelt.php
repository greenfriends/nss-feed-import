<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Nelt
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:nelt:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:nelt:';

    const SUPPLIER_ID = 448;

    private $httpClient;

    private $redis;

    private $source = 'https://b2b.nelt.com/import_artikal_exceles/export';

    private $errors = [];

    public function __construct(\GuzzleHttp\Client $client, \Redis $redis)
    {
        $this->httpClient = $client;
        $this->redis = $redis;
    }

    public function getXml()
    {
        try {
            $data = [
                'userId' => '1110495',
                'userPassword' => '11104951'
            ];
//            $headers = [
//                'Content-Type' => 'text/html'
//            ];
//            $request = new Request('POST', $this->source, $headers, http_build_query($data));
//            $response = $this->httpClient->send($request);
            $xml = $this->curlGetXml($data);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
//        $xml = $response->getBody()->getContents();

        return simplexml_load_string($xml, null, LIBXML_NOCDATA);
    }

    private function curlGetXml($data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->source);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);

        return $server_output;
    }

    public function processItems()
    {
        global $wpdb;
        $existingItems = [];
        $newItems = [];
        $i = 0;
        $products = $this->getXml();
        foreach ($products->children() as $product) {
            $i++;
            $vendorCode = $product->EAN;
            $sql = "SELECT post_id FROM wp_postmeta WHERE meta_key  = 'vendor_code' AND meta_value = '{$vendorCode}'";
            $result = $wpdb->get_results($sql);
            if (!empty($result)) {
                $existingItems[] = $result[0]->post_id;
                $product = $this->parseSource($product, $result[0]->post_id);
                $cacheKey = self::CACHE_KEY_UPDATE;
            } else {
                $newItems[] = $vendorCode;
                $product = $this->parseSource($product);
                $cacheKey = self::CACHE_KEY_CREATE;
            }

            $serializedProduct = serialize($product);
            $key = md5($product->getName() . $product->getRegularPrice() . $product->getImages());
            $this->redis->set($cacheKey . $key, $serializedProduct);
            $this->redis->sAdd($cacheKey . 'index', $key);
        }

        echo '<p>'. count($products) .' items parsed.</p>';
        echo '<p>'. count($existingItems) .' existing items found.</p>';
        echo '<p>'. count($newItems) .' new items found.</p>';
//        echo '<p>'. $storedItems .' items queued for processing.</p>';
        $this->parseErrors();
    }

    function parseSource($product, $postId = false)
    {
        $name = trim((string) $product->WEBName);
        $description = (string) $product->Description;
        $shortdesc = (string) $product->Description;
        $status = 'pending'; // pending
        $stock_status = 'outofstock';
        if((string) $product->NumberOfAvailableProducts === "IMA NA STANJU") {
            $status = 'publish';
            $stock_status = 'instock';
        }
        $type = 'simple';

        $catsData = explode("\n", file_get_contents(__DIR__ . '/vitapur.cats.csv'));
        $categories = $this->getCategories((int) $product->Subcategory, $catsData);
        $images = (string) $product->GraficsURL;
        if ((string) $product->AdditionalGraphicsURL !== '') {
            $images .= ', ' . (string) $product->AdditionalGraphicsURL;
        }

        $dto = [
            'sku' => (string) $product->sku,
            'postId' => $postId,
            'supplierSku' => (string) $product->sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => implode(',', $categories),
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'imageIds' => '',
            'images' => $images,
            'regularPrice' => $this->parsePrice($product->RRP),
            'salePrice' => $this->parsePrice($product->RRP),
            'inputPrice' => $this->parsePrice($product->VPcena),
            'stockStatus' => $stock_status,
            'pdv' => '20',
            'boja' => '',
            'type' => $type,
            'velicina' => '',
            'weight' => (string) $product->tezina,
            'quantity' => '0',
            'manufacturer' => $product->Brand
        ];

        return new Product($dto);
    }

    private function getCategories($productId, $catsData)
    {
        foreach ($catsData as $catDatum) {
            $catId = trim(explode(',', $catDatum)[1]);
            $cat = get_term($catId, 'product_cat');
            if (get_class($cat) !== \WP_Term::class) {
                if ($catId !== '') {
                    $this->errors[] = sprintf('Category with id: %s was not found.', $catId);
                }
                continue;
            }
            $categories = [$cat->term_id];
            if ($cat->parent) {
                $parent = get_term($cat->parent, 'product_cat');
                $categories[] = $parent->term_id;
            }
            if ($parent->parent) {
                $grandParent = get_term($parent->parent, 'product_cat');
                $categories[] = $grandParent->term_id;
            }
        }

        return $categories;
    }

    private function parsePrice($price)
    {
        $parsedPrice = (string) $price;

        $parsedPrice = str_replace('.00', '', $parsedPrice);
        $parsedPrice = str_replace(',','', $parsedPrice);

        return  $parsedPrice;
    }

}