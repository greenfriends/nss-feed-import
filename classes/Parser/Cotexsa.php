<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Cotexsa extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:cotexsa:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:cotexsa:';
    const SUPPLIER_ID = 239259;

    private $catLog = [];

//    protected $source = 'https://www.colossus.rs/wp-content/uploads/woo-product-feed-pro/xml/hjncy4pvbZk7H3OeUVL8oN8UbVckuvyv.xml';
    protected $source = 'http://178.222.126.126/Eshop';

    public function processItems_test()
    {
        global $wpdb;
        $fixedVendorCodes = [];
        foreach ($this->products as $productData) {
            try {
                $product = $this->parseSource($productData);
                $vendorCode = $product->getSupplierSku();
                $sql = "SELECT post_id FROM wp_postmeta pm1 JOIN wp_postmeta pm2 USING(post_id) JOIN wp_posts p ON (pm1.post_id = p.ID)  
                  WHERE p.post_status <> 'trash' AND pm2.meta_key = 'supplier' AND pm2.meta_value = " . static::SUPPLIER_ID
                . " AND p.post_title LIKE '%{$vendorCode}%' 
                AND pm1.meta_key = 'vendor_code' AND pm1.meta_value <> '{$vendorCode}'";

                $postId = $wpdb->get_var($sql);
                $p = wc_get_product($postId);

                if (!$p) {
                    continue;
                }

                if ($p->get_meta('vendor_code') == '') {
                    if (in_array($p->get_id(), $fixedVendorCodes)) {
                        var_dump('double product ?');
                        var_dump($fixedVendorCodes);
                        var_dump($vendorCode);
                        die();
                    }
                    $fixedVendorCodes[] = $p->get_id();
                    $p->update_meta_data('vendorcode', $vendorCode);
                    $p->save();
                }

            } catch (\Exception $e) {
                $this->errors[] = $e->getMessage();
                continue;
            }
        }
        echo 'total fixed vendorcodes: ' . var_dump($fixedVendorCodes);
    }

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->opis;
        $name = (string) $product->ime_proizvoda;
        $sku = (string) $product->sku_proizvoda;
        $stock_status = 'instock';
        if((string) $product->lager !== 'in stock') {
            $stock_status = 'outofstock';
        }
        $type = 'simple';
        $images[] = (string) $product->slika;
        // THE FUCK !?
        $cleanInput = str_replace(' RSD', '', $product->cena);
        $regularPrice = number_format((float) $cleanInput, 0, ',', '');
        $salePrice = '';
        if (isset($product->akcijska_cena)) {
            $salePriceInput = str_replace(' RSD', '', $product->akcijska_cena);
            $salePrice = number_format((float) $salePriceInput, 0, ',', '');
//            if ($salePrice < $regularPrice) {
//                $salePrice = $regularPrice;
//                $regularPrice = $salePrice;
//            }
        }

        //status is only imported first time
        $status = 'publish';
        if ($regularPrice == 0 || $salePrice === 0) {
            var_dump($product->cena);
            var_dump($product->akcijska_cena);
            die();
            $status = 'draft';
        }


        //category mapping template
//        if (!in_array((string) $product->Kategorija, $this->catLog)) {
//            $this->catLog[] = (string) $product->Kategorija;
//            if (isset(explode('||', (string) $product->Kategorija)[2])) {
//                echo explode('||', (string) $product->Kategorija)[1] .','. explode('||', (string) $product->Kategorija)[2] . PHP_EOL;
//            } else {
//                echo ','. explode('||', (string) $product->Kategorija)[1] . PHP_EOL;
//            }
//        }

        $categories = $this->parseCategories(explode('||', (string) $product->kategorija));
        $p = new Product([
            'sku' => $sku,
            'postId' => $postId,
            'supplierSku' => $sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $categories,
            'name' => $name,
            'status' => $status,
            'shortDescription' => '',
            'description' => $description,
            'images' => implode(',', $images),
            'regularPrice' => $regularPrice,
            'salePrice' => $salePrice,
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => 1,
            'manufacturer' => '',
            'boja' => [],
            'type' => $type,
            'velicina' => [],
            'weight' => 0.1,
            'quantity' => 0
        ]);

//        if ($p->getSupplierSku() == 'CSS-5542') {
//            var_dump($p);
//                die();
//        }

        return $p;
    }

    private function parseCategories($cats)
    {
        return $this->fetchCategoryMapping($cats);
    }

    private function fetchCategoryMapping($cats)
    {
        $row = 0;
        $catId = null;
        //@TODO optimize, fetch data once
        $handle = fopen(__DIR__ . "/../../mapping/zomimpex.csv", "rb");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $row++;
            if ($row === 1) {
                continue;
            }
            $str = str_replace(';;;;;;', '', $data[2]);
            if (in_array($str, [';', ';?', ';??', ';???'])) {
                continue;
            }
            foreach ($cats as $cat) {
//            if (in_array($data[0], $cats)) {
                if ($data[0] === $cat) {
                    $catId = explode(';', $str)[0];
                    if (!is_numeric($catId)) {
                        var_dump('tjorak');
                        var_dump($catId);
                        die();
                    }
                    break(2);
                }
            }


        }
        fclose($handle);

        if (!$catId) {
            var_dump($cats);
            die();
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $request = new Request('get', $this->source, ['ApiKey' => '9FE7A52933D4419F9BF823ECD1B6EDDE']);
        $response = $this->getHtpClient()->send($request);
        var_dump(json_decode($request->getBody()->getContents()));
        die();
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->product;
//        $this->products = simplexml_load_string(file_get_contents(__DIR__ . 'prometz-test.xml'), null, LIBXML_NOCDATA)->product;
    }
}
