<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\CategoryMapper;
use Nss\Feed\Product;

abstract class Parser //implements ParserInterface
{
    const CACHE_KEY_CREATE = null;
    const CACHE_KEY_UPDATE = null;

    const SUPPLIER_ID = null;

    private $httpClient;

    protected $redis;

    protected $source;

    protected $products;

    protected $errors = [];

    protected $supplierId;

    /**
     * @var CategoryMapper
     */
    protected $categoryMapping;
    protected $mappedCategories;
    protected $useMapping = false;

    protected $sourceCategories = [];

    public function __construct(\GuzzleHttp\Client $client, \Redis $redis)
    {
        $this->httpClient = $client;
        $this->redis = $redis;
        if ($this->useMapping) {
            $this->fetchCategoryMapping();
        }
        $startTime = $this->getMicrotime();
        $this->fetchItems();
        $endTime = $this->getMicrotime();
        echo 'items fetched in '.($endTime - $startTime).'s' . "\r\n";
//        $this->processItems();
    }

    private function updateCategoryMapping()
    {
        $this->categoryMapping->updateMappingFromFeed($this->mappedCategories, $this->sourceCategories);
    }

    private function fetchCategoryMapping()
    {
        $this->categoryMapping = new CategoryMapper();
        $this->categoryMapping->setSupplierId(static::SUPPLIER_ID);
        $this->mappedCategories = $this->categoryMapping->getMapping();


        // get existing source categories, and add diff from what is found during parsing
//        $this->getMappedSourceCategories();
    }

    public function getProducts()
    {
        return $this->products;
    }


    public function getHtpClient()
    {
        return $this->httpClient;
    }

    abstract protected function fetchItems();

    /**
     * @param $data
     * @param null $id
     * @return Product
     */
    abstract protected function parseSource($data, $id = null);

    private function getMicrotime()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    public function processItems()
    {
        global $wpdb;
        $existingItems = [];
        $newItems = [];
        $startTime = $this->getMicrotime();
        /* @var \Nss\Feed\Product $productData */
        foreach ($this->products as $productData) {
            try {
                $product = $this->parseSource($productData);
                $vendorCode = $product->getSupplierSku();
                $sql = "SELECT post_id FROM wp_postmeta pm1 JOIN wp_postmeta pm2 USING(post_id) JOIN wp_posts p ON (pm1.post_id = p.ID)  
              WHERE p.post_status <> 'trash' AND pm1.meta_key = 'vendor_code' AND pm1.meta_value = '{$vendorCode}'  
              AND pm2.meta_key = 'supplier' AND pm2.meta_value = " . static::SUPPLIER_ID;

                $result = $wpdb->get_results($sql);
            } catch (\Exception $e) {
                $this->errors[] = $e->getMessage();
                continue;
            }

            if (!empty($result)) {
                // duplicated items found in xml......
                if (in_array($vendorCode, $existingItems)) {
                    echo 'duplicated vendor code found';
                    var_dump($vendorCode);
                    die();
                }
                $product->dto['postId'] = $result[0]->post_id;
                $existingItems[] = $result[0]->post_id;
                $cacheKey = static::CACHE_KEY_UPDATE;
            } else {
                $newItems[] = $vendorCode;
                $cacheKey = static::CACHE_KEY_CREATE;
            }

            $serializedProduct = serialize($product);
            $key = md5($product->getName() . $product->getRegularPrice() . $product->getImages());
            $this->redis->set($cacheKey . $key, $serializedProduct);
            $this->redis->sAdd($cacheKey . 'index', $key);
        }
        $endTime = $this->getMicrotime();
        $html = 'items parsed in '.($endTime - $startTime).'s' . "\r\n";

        $html .= '<p>'. count($this->products) .' items parsed.</p>';
        $html .= '<p>'. count($existingItems) .' existing items found.</p>';
        $html .= '<p>'. count($newItems) .' new items found.</p>';

        $key = sprintf('importFeed:%s:#updateTotalSourceItems', \SUPPLIERS[static::SUPPLIER_ID]['name']);
        $this->redis->set($key, count($this->products));
        $key = sprintf('importFeed:%s:#updateExistingItems', \SUPPLIERS[static::SUPPLIER_ID]['name']);
        $this->redis->set($key, count($existingItems));
        $key = sprintf('importFeed:%s:#updateNewItems', \SUPPLIERS[static::SUPPLIER_ID]['name']);
        $this->redis->set($key, count($newItems));

        $vendor = get_class($this);
        $testedVendors = [
            \Nss\Feed\Parser\Sense::class,
            \Nss\Feed\Parser\NewVision::class,
            \Nss\Feed\Parser\Prometz::class,
            \Nss\Feed\Parser\Ctc::class,
        ];
        if (in_array($vendor, $testedVendors)) {
            $count = $this->draftLeftOverItems($existingItems);
            echo '<p>'. $count .' items put offline.</p>';
            $key = sprintf('importFeed:%s:#updateDrafted', \SUPPLIERS[static::SUPPLIER_ID]['name']);
            $this->redis->set($key, $count);
        }
        $html .= $this->parseErrors();
        echo $html;

        $this->sendMailLog($html);
        if ($this->useMapping) {
            $this->updateCategoryMapping();
        }
    }

    public function parseErrors()
    {
        $msg = '';
        if ($this->errorCount()) {
            $msg = sprintf('There was a total of %s errors.', $this->errorCount()) . PHP_EOL;
        }
        $displayedErrors = [];
        foreach ($this->errors as $error) {
            if (!in_array($error, $displayedErrors)) {
                $displayedErrors[] = $error;
                $msg .= $error . PHP_EOL;
            }
        }
        $key = sprintf('importFeed:%s:#updateErrors', \SUPPLIERS[static::SUPPLIER_ID]['name']);
        $this->redis->set($key, serialize($displayedErrors));
        $key = sprintf('importFeed:%s:#updateErrorsCount', \SUPPLIERS[static::SUPPLIER_ID]['name']);
        $this->redis->set($key, $this->errorCount());

        return $msg;
    }

    public function errorCount()
    {
        return count($this->errors);
    }

    protected function getExistingIds()
    {
        return wc_get_products(array(
            'limit' => 10000,
            'meta_key' => 'supplier',
            'meta_value' => static::SUPPLIER_ID,
            'return' => 'ids',
            'status' => ['publish', 'pending', 'private']
        ));
    }

    protected function draftLeftOverItems($itemIds)
    {
//        var_dump(count($this->getExistingIds()));
//        var_dump(count($itemIds));

        $itemsToRemove = array_diff($this->getExistingIds(), $itemIds);
        foreach ($itemsToRemove as $postId) {
            wp_update_post([
                'ID' => $postId,
                'post_status' => 'draft'
            ]);
            var_dump('drafted: ' . $postId);
        }
        return count($itemsToRemove);
    }

    protected function getCatTree($catId)
    {
        $cat = get_term_by('id', $catId, 'product_cat');
        if (!$cat) {
            var_dump('cat not found . ' . $catId);
            die();
        }
        if (!$cat->parent) {
            return $cat->term_id;
        }
        $parent = get_term_by('id', $cat->parent, 'product_cat');
        if (!$parent->parent) {
            return $parent->term_id .','. $cat->term_id;
        }
        $mainCat = get_term_by('id', $parent->parent, 'product_cat');

        return $mainCat->term_id .','. $parent->term_id .','. $cat->term_id;
    }

    public function sendMailLog($message)
    {
        $from = 'mailer@nonstopshop.rs';
        $headers = [
            'Content-Type: text/html; charset=UTF-8',
            "From: NonStopShop <'{$from}'>",
        ];
        $to[] = 'djavolak@mail.ru';
        $subject = 'NSS parse feed cron report - parse items for: ' . SUPPLIERS[static::SUPPLIER_ID]['name'];

        wp_mail($to, $subject, $message, $headers);
    }
}