<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class SaxHome extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:saxhome:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:saxhome:';
    const SUPPLIER_ID = 239267;
    private $catLog = [];

    protected $source = 'http://media.saxhome.rs/2020/07/woocommerce-product-export.csv';

    protected function fetchItems()
    {
        $response = $this->getHtpClient()->get($this->source);

        $this->products = $this->getFormatedProducts($response);
    }

    protected function parseSource($product, $id = null)
    {
        $dto = [
            'sku' => '',
            'postId' => $id,
            'supplierSku' => $product['supplierSku'],
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $product['categories'],
            'name' => $product['name'],
            'status' => 'publish',
            'shortDescription' => $product['shortDescription'],
            'description' => $product['description'],
            'images' => $product['images'],
            'regularPrice' => $product['regularPrice'],
            'salePrice' => $product['salePrice'],
            'inputPrice' => '',
            'stockStatus' => $product['stockStatus'],
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => '',
            'boja' => '',
            'type' => 'simple',
            'velicina' => '',
            'options' => '',
            'weight' => $product['weight'],
            'quantity' => 0
        ];
        return new Product($dto);
    }

    private function getFormatedProducts($response)
    {
        $csv = $this->parse_csv($response->getBody()->getContents());

        $items = [];
        foreach($csv as $key => $row) {
            if ($key < 1) {
                continue;
            }
            $status = true;
            if ($row[5] != 'publish') {
                $status = false;
            }
            $items[] = [
                'name' => ucfirst(mb_strtolower($row[0])),
                'supplierSku' => $row[10],
                'shortDescription' => $row[3],
                'description' => ucfirst(mb_strtolower($row[3])),
                'regularPrice' => $row[14],
                'salePrice' => $row[15],
                'weight' => $row[16],
                'categories' => $row[42],
                'stockStatus' => ($status) ? $row[22] : 'outofstock',
                'images' => explode(' | ', $row[38]),
            ];
            //category mapping template
//            if (!in_array($row[42], $this->catLog)) {
//                $this->catLog[] = $row[42];
//                echo $row[42] . PHP_EOL;
//            }
        }
        return $items;
    }

    private function parse_csv($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true)
    {
        $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
        $enc = preg_replace_callback(
            '/"(.*?)"/s',
            function ($field) {
                return urlencode(utf8_encode($field[1]));
            },
            $enc
        );
        $lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);
        return array_map(
            function ($line) use ($delimiter, $trim_fields) {
                $fields = $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line);
                return array_map(
                    function ($field) {
                        return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
                    },
                    $fields
                );
            },
            $lines
        );
    }
}