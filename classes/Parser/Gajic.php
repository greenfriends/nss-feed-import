<?php

namespace Nss\Feed\Parser;

use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class Gajic extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:gajic:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:gajic:';
    const SUPPLIER_ID = 297;

    protected $source = 'https://msgajic.rs/portali.xml';

    protected $catLog = [];

    /**
     * @param \SimpleXMLElement $product
     * @return bool|Product
     * @throws \Exception
     */
    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->short_description;
        $shortdesc = $description;
        $name = (string) $product->name;
        $stock_status = 'instock';
        $status = 'publish';
//        if((int) $product->stock_quantity < 1) {
//            $stock_status = 'outofstock';
//        }
        $type = 'simple';
        $images = (string) $product->image_url;
        $regularPrice = (string) $product->price;
        $options = [];
        $varijacije = explode(',', (string) $product->size);
        if (!empty($varijacije)) {
            $type = 'variable';
            $i = 0;
            foreach ($varijacije as $varijacija) {
                $options[$i]['stockStatus'] = $stock_status;
                $options[$i]['regularPrice'] = $regularPrice;
                $options[$i]['salePrice'] = '';
                $options[$i]['velicina'] = [
                    'value' => trim($varijacija),
                ];
                $i++;
            }
        }

        //category mapping template
//        if (!in_array((string) $product->kategorija, $this->catLog)) {
//            $this->catLog[] = (string) $product->kategorija;
//            echo ',' . $product->kategorija . PHP_EOL;
//        }
//        $categories = $this->parseCategories((string) $product->kategorija);

        $dto = [
            'sku' => '',
            'postId' => $postId,
            'supplierSku' => (string) $product->sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => '3152',
            'name' => $name,
            'status' => $status,
            'shortDescription' => $shortdesc,
            'description' => $description,
            'images' => $images,
            'regularPrice' => $regularPrice,
            'salePrice' => '',
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'type' => $type,
            'options' => $options,
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => '',
            'boja' => '',
            'velicina' => '',
            'weight' => 0.1,
            'quantity' => 0
        ];

        return new Product($dto);
    }

    private function parseCategories($cats)
    {
        $row = 0;
        $catId = null;
        //@TODO optimize, fetch data before parsing items
        $handle = fopen(__DIR__ . "/../../mapping/prometz.csv", "rb");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $row++;
            if ($row === 1) {
                continue;
            }
            if ($data[0] === $cats) {
                if (is_numeric($data[2])) {
                    $catId = $data[2];
                    break;
                }
            }
        }
        fclose($handle);

        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $response = $this->getHtpClient()->send(new Request('get', $this->source));
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->xpath('//item');
//        $this->products = simplexml_load_string(file_get_contents(__DIR__ . '/prometz-test.xml'), null, LIBXML_NOCDATA)->xpath('//Proizvod');
    }
}