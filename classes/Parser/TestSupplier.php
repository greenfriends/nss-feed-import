<?php


namespace Nss\Feed\Parser;


use GuzzleHttp\Psr7\Request;
use Nss\Feed\Product;

class TestSupplier extends Parser
{
    const CACHE_KEY_CREATE = 'importFeedQueueCreate:testSupplier:';
    const CACHE_KEY_UPDATE = 'importFeedQueueUpdate:testSupplier:';
    const SUPPLIER_ID = 239262;

    private $catLog = [];
    protected $useMapping = true;

    protected $source = 'https://nss-devel.ha.rs/wp-content/uploads/testImport.xml';

    public function parseSource($product, $postId = false)
    {
        $description = (string) $product->opis;
        $name = (string) $product->ime_proizvoda;
        $sku = (string) $product->sku_proizvoda;
        $stock_status = 'instock';
        if((string) $product->lager !== 'in stock') {
            $stock_status = 'outofstock';
        }
        $type = 'simple';
        $images[] = (string) $product->slika;
        // THE FUCK !?
        $cleanInput = str_replace(' RSD', '', $product->cena);
        $regularPrice = number_format((float) $cleanInput, 0, ',', '');
        $salePrice = '';
        if (isset($product->akcijska_cena)) {
            $salePriceInput = str_replace(' RSD', '', $product->akcijska_cena);
            $salePrice = number_format((float) $salePriceInput, 0, ',', '');
//            if ($salePrice < $regularPrice) {
//                $salePrice = $regularPrice;
//                $regularPrice = $salePrice;
//            }
        }

        //status is only imported first time
        $status = 'publish';
        if ($regularPrice == 0 || $salePrice === 0) {
            var_dump($product->cena);
            var_dump($product->akcijska_cena);
            die();
            $status = 'draft';
        }

        $cats = [];
        foreach (explode('||', (string) $product->kategorija) as $cat) {
            if (!strlen(trim($cat))) {
                continue;
            }
            $cats[] = htmlspecialchars_decode($cat);
        }
        $catString = implode('###', $cats);
        $catString = str_replace('|', '###', $catString);
        if (!in_array($catString, $this->sourceCategories)) {
            $this->sourceCategories[] = $catString;
        }

        $p = new Product([
            'sku' => $sku,
            'postId' => $postId,
            'supplierSku' => $sku,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $this->parseCategories($cats),
            'name' => $name,
            'status' => $status,
            'shortDescription' => '',
            'description' => $description,
            'images' => implode(',', $images),
            'regularPrice' => $regularPrice,
            'salePrice' => $salePrice,
            'inputPrice' => '',
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => 1,
            'manufacturer' => '',
            'boja' => [],
            'type' => $type,
            'velicina' => [],
            'weight' => 0.1,
            'quantity' => 0
        ]);

        return $p;
    }

    private function parseCategories($cats)
    {
        $catId = null;
        foreach ($this->mappedCategories->getIterator() as $row => $item) {
            if ($row === 0) {
                continue;
            }
            if ($cats[0] === $item['source1']) {
                if (isset($cats[1])) {
                    if ($cats[1] !== $item['source2']) {
                        continue;
                    }
                    $catId = $item['localId1'];
                }
            }
        }

        if (!$catId) {
            throw new \Exception('No category mapped for this item.');
        }

        return $this->getCatTree($catId);
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $response = $this->getHtpClient()->send(new Request('get', $this->source));
        $this->products = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_NOCDATA)->product;
//        $this->products = simplexml_load_string(file_get_contents(__DIR__ . 'prometz-test.xml'), null, LIBXML_NOCDATA)->product;
    }
}