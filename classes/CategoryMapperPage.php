<?php

namespace Nss\Feed;
use League\Csv\Reader;
use League\Csv\Writer;

class CategoryMapperPage
{
    public $ignoredVendors = [123, 224, 296, 198];

    /**
     * @var CategoryMapper
     */
    private $categoryMapper;

    public function __construct()
    {
        $this->categoryMapper = new CategoryMapper();
    }

    public function init()
    {
//        $this->categoryMapper = new CategoryMapper();
//        add_action('admin_menu', function () {
//            add_submenu_page('nss-panel', 'Mapiranje kategorija za feed', 'Mapiranje kategorija za feed',
//                'edit_pages', 'nss-feed-mapper', [$this, 'getView'], 999);
//        });
//        ini_set('max_input_vars', '5000');
//        ini_set('memory_limit', '512M');
    }

    public function getView()
    {
        $categories = $this->getCategories();
        $catList = false;
        $msg = false;
        if (isset($_GET['supplierId'])) {
//            $this->migrateOldFile(119);
            $this->categoryMapper->setSupplierId($_GET['supplierId']);
            if (!in_array($_GET['supplierId'], $this->ignoredVendors)) {
                try {
                    if (isset($_POST['saveMapping'])) {
                        $this->saveCsvFromPost();
                        $msg = 'saved data';
                    }
                    $catList = $this->categoryMapper->getMapping();
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                }
            } else {
                $msg = sprintf('Dobavljač <em>%s</em> ne koristi mapiranje.', SUPPLIERS[$_GET['supplierId']]['name']);
            }
        }

        include(__DIR__ . '/../view/categoryMapper.php');
    }

    private function getCategories()
    {
        $categories = [];
        foreach (\Gf\Util\CategoryFunctions::gf_get_categories() as $category) {
            if ($category->parent == 0) {
                continue;
            }
            $categories[] = $category;
        }


         return $categories;
    }

    private function saveCsvFromPost()
    {
        $limit = count($_POST['source1']);
        $data = [];
        for ($i=0; $i<$limit; $i++) {
            $data[] = [
                $_POST['source1'][$i], $_POST['source2'][$i], $_POST['localId1'][$i], $_POST['localId2'][$i]
            ];
        }
        $this->categoryMapper->saveMappingToCsv($data);
    }

    /**
     * @deprecated
     * @param $supplierId
     */
    private function migrateOldFile($supplierId) {
        $mappingFile = sprintf('%s/../mapping/%s.csv', __DIR__, SUPPLIERS[$supplierId]['name']);
        $migratedMappingFile = sprintf('%s/uploads/feed/mapping/%s-new.csv', WP_CONTENT_DIR, SUPPLIERS[$supplierId]['name']);
        $csv = Reader::createFromPath($mappingFile, 'r');
        $csv->setHeaderOffset(0);

        $writer = Writer::createFromString();
        $writer->insertOne(['source1', 'source2', 'localId1', 'localId2']);
        $data = [];
        foreach ($csv->getRecords() as $record) {
            $data[] = [
//                $record['source1'], $record['source2'], $record['localId1'], $record['localId2']
                $record['source1'], $record['source2'], explode(';', str_replace(';;;;;;', '', $record['localId1;;;;;;;']))[0], ''
            ];
        }
        $writer->insertAll($data);
        var_dump(file_put_contents($migratedMappingFile, $writer->toString()));

        echo 'done';
    }
}

//$mapper = new CategoryMapperPage();
//$mapper->init();