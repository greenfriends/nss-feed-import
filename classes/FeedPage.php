<?php

namespace Nss\Feed;


class FeedPage
{

    public function init()
    {
        add_action('admin_menu', function () {
            add_submenu_page('nss-panel', 'Pregled feedova', 'Feedovi',
                'edit_pages', 'nss-feed', [$this, 'getView'], 999);
        });
        ini_set('max_input_vars', '5000');
        ini_set('memory_limit', '512M');
    }

    public function getView()
    {
        $action = 'status';
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }

        switch ($action) {
            case 'history':
                $page = new NssFeedHistory();
                break;

            case 'mapping':
                $page = new CategoryMapperPage();
                break;

            case 'status':
                $page = new FeedStatusPage();
                break;
        }

        $page->getView();
    }

}

$feedPage = new FeedPage();
$feedPage->init();