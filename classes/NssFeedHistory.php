<?php

namespace Nss\Feed;

class NssFeedHistory
{
    public function init()
    {
//        add_action('admin_menu', function () {
//            add_submenu_page('nss-panel', 'Promene sa feeda', 'Promene sa feeda',
//                'edit_pages', 'nss-feed-history', [$this, 'getView'], 999);
//        });
        add_action("wp_ajax_getLogData", [$this, 'getData']);
        add_action('admin_enqueue_scripts', [$this, 'setupScriptsAndStyles']);
    }

    public function getView()
    {
        include(__DIR__ . '/../view/logList.php');
    }

    public function getData()
    {
        global $wpdb;
        $search = $_GET['search']['value'] ?? null;
        $supplierFilter = $_GET['supplierId'] ?? '-1';
        $limit = $_GET['length'];
        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $sql ="SELECT * FROM wp_nss_feed_log";
        $totalCount = (int)$wpdb->get_var("SELECT COUNT(*) FROM wp_nss_feed_log");
        $recordsFiltered = $totalCount;
        $searchQuery = '';

        if ($search !== null && $search !== ''){
            $searchQuery = " WHERE logId like '{$search}' OR productId like '{$search}'";
            $sql .= $searchQuery;
            $recordsFiltered = $wpdb->get_var("SELECT COUNT(*) FROM wp_nss_feed_log {$searchQuery}");
        }
        if ($supplierFilter !== '-1') {
            $operator = ' WHERE ';
            if ($search !== null && $search !== ''){
                $operator = ' AND ';
            }
            $filterQuery = "{$operator}supplierId = '{$supplierFilter}'";
            $filterQueryCount = $searchQuery.$filterQuery;
            $sql .= $filterQuery;
            $recordsFiltered = $wpdb->get_var("SELECT COUNT(*) FROM wp_nss_feed_log {$filterQueryCount}");
        }
        $sql .= " ORDER BY logId DESC LIMIT {$offset}, {$limit}";
        $data = $wpdb->get_results($sql);
        $formattedData = [];
        foreach ($data as $logEntity) {
            $supplierId = $logEntity->supplierId ?? get_post_meta($logEntity->productId,'supplier',true);
            $catNamesNew = null;
            $catNamesOld = null;
            if ($logEntity->attribute === 'category'){
                $oldCategoryIds = explode(',', $logEntity->oldValue);
                $newCategoryIds = explode(',', $logEntity->newValue);
                foreach ($oldCategoryIds as $catId) {
                    $catNamesOld[] = get_term($catId, 'product_cat')->name;
                }
                foreach ($newCategoryIds as $catId) {
                    $catNamesNew[] = get_term($catId, 'product_cat')->name;
                }
                $catNamesNew = implode(',', $catNamesNew);
                $catNamesOld = implode(',', $catNamesOld);
            }
            $formattedData[] = [
                'logId' => $logEntity->logId,
                'productId' => $logEntity->productId,
                'type' => $logEntity->type,
                'attribute' =>$logEntity->attribute,
                'message' =>$logEntity->message,
                'oldValue' =>$catNamesOld ?? $logEntity->oldValue,
                'newValue' => $catNamesNew ?? $logEntity->newValue,
                'timestamp' => $logEntity->timestamp,
                'supplierId' => $supplierId,
                'supplierName' => get_user_by('ID',$supplierId)->display_name
            ];
        }
        $response = [
            'draw' => $draw,
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $recordsFiltered,
            'data' => $formattedData
        ];
        wp_send_json($response);
    }


    public function setupScriptsAndStyles()
    {
        wp_enqueue_style('datatableCss', '//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css');
        wp_enqueue_script('datatablejs', '//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js', ['jquery']);
    }
}

$feedHistory = new NssFeedHistory();
$feedHistory->init();