<?php

namespace Nss\Feed;


class FeedFactory
{
    public function __invoke()
    {
        global $wpdb;

        $httpClient = new \GuzzleHttp\Client(['defaults' => [
            'verify' => false
        ]]);
        $redis = new \Redis();
        $redis->connect(REDIS_HOST);

        return new Feed($httpClient, $redis, $wpdb);
    }
}