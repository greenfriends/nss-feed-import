<?php

namespace Nss\Feed;

use League\Csv\Reader;
use League\Csv\Writer;

class CategoryMapper
{
    private $supplierId;

    private $storage;

    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        $this->storage = sprintf(
            '%s/uploads/feed/mapping/%s-new.csv', WP_CONTENT_DIR, \SUPPLIERS[$supplierId]['name']
        );
    }

    public function updateMappingFromFeed($mappedCategories, $sourceCategories)
    {
        $mapped = [];
        $data = [];
        $diff = [];
        foreach ($mappedCategories as $item) {
            if (!strlen($item['source2'])) {
                $string = $item['source1'];
            } else {
                $string = implode('###', [$item['source1'], $item['source2']]);
            }
            // category removed from source
            if (!in_array($string, $sourceCategories)) {
                continue;
            }
            $mapped[] = $string;
            $data[] = [$item['source1'], $item['source2'], $item['localId1'], $item['localId2']];
        }
//        var_dump(count($sourceCategories));
//        var_dump(count($mapped));
//        var_dump(count($data));
//        var_dump(count($mappedCategories));

        $diff = array_diff($sourceCategories, $mapped);
        foreach ($diff as $item) {
            $parts = explode('###', $item);
//            $part1 = '';
            $part0 = $parts[0];
            unset($parts[0]);
            $part1 = implode('|', $parts);
            $data[] = [$part0, $part1, '', ''];
        }
//        var_dump($data);
//        var_dump($diff);
//        die();

        if (count($diff)) {
//            var_dump($sourceCategories);
//            var_dump($diff);
//            die();
            $this->saveMappingToCsv($data);
        }
    }

    private function checkSupplierIdIsSet()
    {
        if (!$this->supplierId) {
            throw new \Exception('setSupplierId has to be called first.');
        }
    }

    /**
     * @param $data [source1, source2, localId1, localId2]
     * @return bool|int
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function saveMappingToCsv($data)
    {
        $this->checkSupplierIdIsSet();
        $writer = Writer::createFromString();
        $writer->insertOne(['source1', 'source2', 'localId1', 'localId2']);
        $writer->insertAll($data);

        return file_put_contents($this->storage, $writer->toString());
    }

    /**
     * @param $supplierId
     * @return static
     * @throws \Exception
     * @throws \League\Csv\Exception
     */
    public function getMapping()
    {
        $this->checkSupplierIdIsSet();

        if (!file_exists($this->storage)) {
//            throw new \Exception('Nije kreirano mapiranje za dobavljaca : ' . \SUPPLIERS[$this->supplierId]['name']);
            $this->saveMappingToCsv([]);
        }
        $catList = Reader::createFromPath($this->storage, 'r');
        $catList->setHeaderOffset(0);

        return $catList;
    }

}