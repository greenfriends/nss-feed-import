<?php

namespace Nss\Feed;

use \Nss\Feed\Importer;
use \Nss\Feed\ParserFactory;
use \GuzzleHttp\Client;

class Feed
{
    private $redis;

    private $httpClient;

    private $wpdb;

    private $parser;

    public function __construct(Client $httpClient, \Redis $redis, \wpdb $wpdb)
    {
        $this->redis = $redis;
        $this->httpClient = $httpClient;
        $this->wpdb = $wpdb;
    }

    public function importNew($args)
    {
        $key = 'importFeedQueueCreate:' . SUPPLIERS[$args[0]]['name'] .':';

        return $this->startImport($args, $key);
    }

    public function importExisting($args)
    {
        $key = 'importFeedQueueUpdate:' . SUPPLIERS[$args[0]]['name'] .':';

        return $this->startImport($args, $key);
    }

    public function startImport($args, $key)
    {
        $offset = 0;
        $limit = 2000;
        if (isset($args[1])) {
            $offset = $args[1];
        }
        if (isset($args[2])) {
            $limit = $args[2];
        }
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key);

        return $importer->importItems($offset, $limit, SUPPLIERS[$args[0]]['name']);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function parseFeed($args)
    {
        $this->parser = ParserFactory::make(SUPPLIERS[$args[0]], $this->httpClient, $this->redis);

        return $this->parser->processItems();
    }

    public function resetQueue($args)
    {
        $key = 'importFeedQueueUpdate:' . SUPPLIERS[$args[0]]['name'] .':';
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key);
        $importer->resetQueue();

        $key = 'importFeedQueueCreate:' . SUPPLIERS[$args[0]]['name'] .':';
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key);
        $importer->resetQueue();
    }

    public function getErrors()
    {
        return $this->parser->parseErrors();
    }
}